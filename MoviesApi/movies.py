from app import app
from services.MoviesService import MoviesService
from flask import jsonify
from flask import flash, request
from werkzeug import generate_password_hash, check_password_hash

movies_service = MoviesService()

@app.route('/movie', methods=['POST'])
def add_movie():
    try:
        _json = request.json
        _name = _json['name']
        _description = _json['description']
        _stars = _json['stars']
        _year = _json['year']
        # validate the received values
        if _name and request.method == 'POST':
            lastrowid = movies_service.add_movies(_name, _description, _stars, _year)
            resp = jsonify({'id': lastrowid})
            resp.status_code = 200
            return resp
        else:
            return not_found()
    except Exception as e:
        print(e)

@app.route('/movie', methods=['GET'])
def get_all_movies():
    try:
        page = request.args.get('page', default = 1, type = int)
        name = request.args.get('name', default = None, type = str)
        app.logger.info("page: " + str(page))
        pagesize = 2
        rows = movies_service.get_all_movies(page, pagesize, name)
        resp = jsonify(rows)
        resp.status_code = 200
        return resp
    except Exception as e:
        print(e)

@app.route('/movie/<int:id>', methods=['GET'])
def get_movies_by_id(id):
    try:
        row = movies_service.get_movies_by_id(id)
        resp = jsonify(row)
        resp.status_code = 200
        return resp
    except Exception as e:
        print(e)

@app.route('/movie/<int:id>', methods=['PUT'])
def update_movie(id):
    try:
        _json = request.json
        _name = _json['name']
        _description = _json['description']
        _stars = _json['stars']
        _year = _json['year']
        # validate the received values
        if _name and id and request.method == 'PUT':
            rows_affected = movies_service.update_movie(id, _name, _description, _stars, _year)
            app.logger.info("PUT update_Movie, rows_affected: " + str(rows_affected))
            if not any(rows_affected):
                resp = jsonify({'message': 'Movie was not updated!'})
                resp.status_code = 200
            else:
                resp = jsonify({'message': 'Movie updated successfully!'})
                resp.status_code = 200
            return resp
        else:
            return not_found()
    except Exception as e:
        print(e)

@app.route('/movie/<int:id>', methods=['DELETE'])
def delete_movie(id):
    try:
        rows_affected = movies_service.delete_movie(id)
        if rows_affected == 0:
            resp = jsonify({'message': 'Movie was not deleted!'})
            resp.status_code = 200
        else:
            resp = jsonify({'message': 'Movie deleted successfully!'})
            resp.status_code = 200
        return resp
    except Exception as e:
        print(e)
