from repositories.MoviesRepository import MoviesRepository

class MoviesService(object):
    def __init__(self):
        self.movies_repository = MoviesRepository()

    def add_movies(self, name, description, stars, year):
        return self.movies_repository.add_movies(name, description, stars, year)

    def get_all_movies(self, page, pagesize, name):
        print("...service....name: " +name)
        return self.movies_repository.get_all_movies(page, pagesize, name)

    def get_movies_by_id(self, id):
        return self.movies_repository.get_movies_by_id(id)

    def update_movie(self, id, name, description, stars, year):
        nameOp, descOp, starsOp, yearOp = None, None, None, None
        if name:
            nameOp = self.movies_repository.update_movie_name(id, name)
        if description:
            descOp = self.movies_repository.update_movie_description(id, description)
        if stars:
            starsOp = self.movies_repository.update_movie_stars(id, stars)
        if year:
            yearOp = self.movies_repository.update_movie_year(id, year)
        stateOp = (nameOp, descOp, starsOp, yearOp)
        return stateOp

    def delete_movie(self, id):
        return self.movies_repository.delete_movie(id)
