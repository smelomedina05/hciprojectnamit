import pymysql
from db_config import mysql

class MoviesRepository(object):
    def __init__(self):
        self.conn = mysql.connect()

    def add_movies(self, name, description, stars, year):
        sql = "insert into movies(name, description, stars, year) values ('{}', '{}', {}, {});".format(name, description, stars, year)
        cursor = self.conn.cursor()
        cursor.execute(sql)
        self.conn.commit()
        lastrowid = cursor.lastrowid
        cursor.close()
        return lastrowid

    def get_all_movies(self, page, pagesize, name):
        print("...repo....name: " +name)
        startat = page*pagesize
        cursor = self.conn.cursor(pymysql.cursors.DictCursor)
        if name:
            cursor.execute("SELECT * FROM movies WHERE name LIKE %s LIMIT %s, %s", (name,startat, pagesize))
        else:
            cursor.execute("SELECT * FROM movies LIMIT %s, %s", (startat, pagesize))
        rows = cursor.fetchall()
        cursor.close()
        return rows

    def get_movies_by_id(self, id):
        cursor = self.conn.cursor(pymysql.cursors.DictCursor)
        cursor.execute("SELECT * FROM movies WHERE id=%s", id)
        row = cursor.fetchone()
        cursor.close()
        return row

    def update_movie_name(self, id, name):
        sql = "UPDATE movies SET name='{}' WHERE id='{}'".format(name, id)
        cursor = self.conn.cursor()
        rows_affected = cursor.execute(sql, data)
        self.conn.commit()
        cursor.close()
        return rows_affected

    def update_movie_description(self, id, description):
        sql = "UPDATE movies SET description='{}' WHERE id='{}'".format(description, id)
        cursor = self.conn.cursor()
        rows_affected = cursor.execute(sql, data)
        self.conn.commit()
        cursor.close()
        return rows_affected

    def update_movie_stars(self, id, stars):
        sql = "UPDATE movies SET stars='{}' WHERE id='{}'".format(stars, id)
        cursor = self.conn.cursor()
        rows_affected = cursor.execute(sql, data)
        self.conn.commit()
        cursor.close()
        return rows_affected

    def update_movie_year(self, id, year):
        sql = "UPDATE movies SET year='{}' WHERE id='{}'".format(year, id)
        cursor = self.conn.cursor()
        rows_affected = cursor.execute(sql, data)
        self.conn.commit()
        cursor.close()
        return rows_affected

    def delete_movie(self, id):
        cursor = self.conn.cursor()
        rows_affected = cursor.execute("DELETE FROM movies WHERE id=%s", (id,))
        self.conn.commit()
        cursor.close()
        return rows_affected
