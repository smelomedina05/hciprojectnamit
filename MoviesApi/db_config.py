from app import app
from flaskext.mysql import MySQL
from flask_jwt_extended import JWTManager

mysql = MySQL()

# MySQL configurations
app.config['MYSQL_DATABASE_USER'] = 'root'
app.config['MYSQL_DATABASE_PASSWORD'] = '1234321'
app.config['MYSQL_DATABASE_DB'] = 'DBMovies'
app.config['MYSQL_DATABASE_HOST'] = '172.18.0.2'   #'172.20.0.2' #'MySQLMoviesDB'
mysql.init_app(app)

app.config['JWT_SECRET_KEY'] = 'mySecretKey'  # The same secret of the java service
app.config['JWT_ALGORITHM'] = 'HS512'
jwt = JWTManager(app)
